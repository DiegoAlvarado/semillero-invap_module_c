#include <stdio.h>
#include <stdlib.h>

struct Equipo {
  char Nombre[20];
  int victorias;
  int empates;
  int derrotas;
  int goles;
  struct Equipo *sig;
};

struct Equipo *reservarMemoria() {
  struct Equipo *q;
  q = (struct Equipo *)malloc(sizeof(struct Equipo));
  if (!q) {
    printf("No se ha reservado memoria para el nuevo nodo");
  }
  return q; // devuelve la direcc. de memomoria reservada
}
void LlenarDatos(int cantidad, struct Equipo *lista_equipos);
void ImprimirLista(int cantidad, struct Equipo *lista_equipos);

int main() {
  int cantidad;
  struct Equipo *lista_equipos;
  lista_equipos = NULL;
  int ordenamiento;

  printf("Ingrese numero de equipos:\n");
  scanf("%d", &cantidad);

  for (int i = 0; i < cantidad; i++) {
    lista_equipos = reservarMemoria();
    lista_equipos->sig = NULL;
  }
  for (int i = 0; i < cantidad; i++) {
    printf("lista: %d\n", lista_equipos->goles);
    lista_equipos->sig = NULL;
  }
  LlenarDatos(cantidad, lista_equipos);

  printf("Ingrese método de ordenamiento:\n");
  printf("Victorias = 1\n");
  printf("Derrotas = 2\n");
  printf("Goles = 3\n");
  scanf("%d", &ordenamiento);
  int temp;
  switch (ordenamiento) {
  case 1:
    for (int i = 1; i < cantidad; i++) {
      for (int j = 0; j < cantidad - i; j++) {
        if (lista_equipos[j].victorias < lista_equipos[j + 1].victorias) {
          struct Equipo temp = lista_equipos[i];
          lista_equipos[i] = lista_equipos[j];
          lista_equipos[j] = temp;
        }
      }
    }
    ImprimirLista(cantidad, lista_equipos);
    break;
  case 2:
    for (int i = 1; i < cantidad; i++) {
      for (int j = 0; j < cantidad - i; j++) {
        if (lista_equipos[j].derrotas < lista_equipos[j + 1].derrotas) {
          struct Equipo temp = lista_equipos[i];
          lista_equipos[i] = lista_equipos[j];
          lista_equipos[j] = temp;
        }
      }
    }
    ImprimirLista(cantidad, lista_equipos);
    break;
  case 3:
    for (int i = 1; i < cantidad; i++) {
      for (int j = 0; j < cantidad - i; j++) {
        if (lista_equipos[j].goles < lista_equipos[j + 1].goles) {
          struct Equipo temp = lista_equipos[i];
          lista_equipos[i] = lista_equipos[j];
          lista_equipos[j] = temp;
        }
      }
    }
    ImprimirLista(cantidad, lista_equipos);
    break;
  }

  free(lista_equipos);
  return 0;
}

void LlenarDatos(int cantidad, struct Equipo *lista_equipos) {
  for (int i = 0; i < cantidad; i++) {
    printf("Ingrese nombre de equipo:\n");
    scanf("%s", lista_equipos[i].Nombre);
    printf("Ingrese numero de victorias:\n");
    scanf("%d", &lista_equipos[i].victorias);
    printf("Ingrese numero de empates:\n");
    scanf("%d", &lista_equipos[i].empates);
    printf("Ingrese numero de derrotas:\n");
    scanf("%d", &lista_equipos[i].derrotas);
    printf("ingrese cantidad de goles\n");
    scanf("%d", &lista_equipos[i].goles);
  }
  ImprimirLista(cantidad, lista_equipos);
}

void ImprimirLista(int cantidad, struct Equipo *lista_equipos) {
  for (int i = 0; i < cantidad; i++) {
    printf("nombre: %s\n", lista_equipos[i].Nombre);
    printf("victorias: %d\n", lista_equipos[i].victorias);
    printf("empates: %d\n", lista_equipos[i].empates);
    printf("derrotas: %d\n", lista_equipos[i].derrotas);
    printf("goles: %d\n", lista_equipos[i].goles);
    lista_equipos->sig = NULL;
  }
  printf("\n");
}