#include <stdio.h>
#include <string.h>

int main() {
  FILE *file;
  char x;
  int cont = 0;
  file = fopen("lorem_ipsum.txt", "rb");
  if (file == NULL) {
    printf("El archivo no se encuentra\n");
  } else {
    while (!feof(file)) {
      x = fgetc(file);
      if (x == ' ' || x == '\n' || x == '\r') {
        cont++;
      }
    }
  }
  printf("Este texto contiene: %d palabras\n", cont);
  fclose(file);

  return 0;
}