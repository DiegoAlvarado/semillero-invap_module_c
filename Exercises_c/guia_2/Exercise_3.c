#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct Jugador {
  char nombre[30];
  int cantidadDePuntos;
  bool ganar;
  bool pedirCartas;
} Jugador;

void Repartir(char *Mazo, int *tamanyo, Jugador *actual);
int Aleatorio(int minimo, int maximo);
void EliminarCartaDeBaraja(char *Mazo, int *tamanyo, int elem);
void MostrarCartas(Jugador *actual);
void Ganador(Jugador *vec);

int main() {
  srand(time(NULL));
  char Mazo[] = "23456789JQKA23456789JQKA23456789JQKA23456789JQKA";
  Jugador vec[2];
  vec[0].cantidadDePuntos = 0;
  vec[1].cantidadDePuntos = 0;
  vec[0].ganar = false;
  vec[1].ganar = false;
  bool pedirCarta = true;
  char otraCarta;
  int tamanyoMazo = strlen(Mazo);
  int flag = 0;
  printf("Bienvenido al BlackJack para dos jugadores\n");
  printf("Mazo: %s \n", Mazo);
  printf("Ingrese nombre jugador 1\n");
  scanf("%s", vec[0].nombre);
  printf("Ingrese nombre jugador 2\n");
  scanf("%s", vec[1].nombre);
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++) {
      Repartir(Mazo, &tamanyoMazo, &vec[j]);
    }
  }
  MostrarCartas(&vec[0]);
  MostrarCartas(&vec[1]);
  printf("cartas restantes: ");
  for (int i = 0; i < tamanyoMazo; i++) {
    printf("%c", Mazo[i]);
  }
  printf("\n");
  while (flag != 1) {
    for (int i = 0; i < 2; i++) {
      printf("Jugador %s ¿Desea otra carta? S=si/N=no\n", vec[i].nombre);
      scanf(" %c", &otraCarta);
      switch (otraCarta) {
      case 'S':
      case 's':
        Repartir(Mazo, &tamanyoMazo, &vec[i]);
        MostrarCartas(&vec[i]);
        vec[i].pedirCartas = true;
        break;
      case 'N':
      case 'n':
        vec[i].pedirCartas = false;
        MostrarCartas(&vec[i]);
        break;
      }
    }
    if (vec[0].pedirCartas == false && vec[1].pedirCartas == false) {
      Ganador(vec);
    }
    for (int i = 0; i < 2; i++) {
      if (vec[i].ganar == true) {
        printf("¡Jugador %s gana!\n", vec[i].nombre);
        flag = 1;
      }
    }
  }
  printf("fin del juego");
  return 0;
}

int Aleatorio(int minimo, int maximo) {
  int aleatorio = minimo + rand() / (RAND_MAX / (maximo - minimo + 1) + 1);
  return aleatorio;
}

void Repartir(char *Mazo, int *tamanyo, Jugador *actual) {
  int posicion = Aleatorio(0, *tamanyo - 1);
  int elem = Mazo[posicion] - '0';
  printf("valor de carta = %d \n", elem);
  if (elem == 'A' - '0') {
    int aux;
    MostrarCartas(actual);
    printf("elija valor de As 1=1 o 2=11\n");
    scanf("%d", &aux);
    if (aux == 1)
      elem = 1;
    else if (aux == 2)
      elem = 11;
  } else if (elem == 'J' - '0' || elem == 'Q' - '0' || elem == 'K' - '0') {
    elem = 10;
  }
  actual->cantidadDePuntos += elem;
  EliminarCartaDeBaraja(Mazo, tamanyo, posicion);
  printf("cartas en el mazo: %d \n", *tamanyo);
}

void EliminarCartaDeBaraja(char *Mazo, int *tamanyo, int elem) {
  int i;
  for (i = elem; i < *tamanyo - 1; i += 1) {
    Mazo[i] = Mazo[i + 1];
  }
  *tamanyo -= 1;
}

void MostrarCartas(Jugador *actual) {
  printf("puntos acumulados jugador %s : %d \n", actual->nombre,
         actual->cantidadDePuntos);
}
void Ganador(Jugador *vec) {
  if (vec[0].cantidadDePuntos > vec[1].cantidadDePuntos &&
      vec[0].cantidadDePuntos <= 21) {
    vec[0].ganar = true;
    vec[1].ganar = false;
  }
  if (vec[1].cantidadDePuntos > vec[0].cantidadDePuntos &&
      vec[1].cantidadDePuntos <= 21) {
    vec[0].ganar = false;
    vec[1].ganar = true;
  }
  if (vec[0].cantidadDePuntos > 21) {
    vec[0].ganar = false;
    vec[1].ganar = true;
  }
  if (vec[1].cantidadDePuntos > 21) {
    vec[0].ganar = true;
    vec[1].ganar = false;
  }
}