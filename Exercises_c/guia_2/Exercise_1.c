#include <stdio.h>
#include <stdlib.h>

int main() {
  float vector[] = {4.7, -8.0, -2.3, 11.4, 12.9, 5.1, 8.8, -0.2, 6.0, -14.7};
  int l = 10;
  int temp;
  int n;
  int metodo;
  printf("vector a ordenar:\n");
  for (int i = 0; i < l; i++) {
    printf("%.2f ", vector[i]);
  }
  printf("\n");
  printf("Seleccione el metodo de ordenamiento\n");
  printf("menor a mayor en valor absoluto = 1\n");
  printf("menor a mayor algebraicamente = 2\n");
  printf("mayor a menor en valor absoluto = 3\n");
  printf("mayor a menor algebraicamente = 4\n");
  scanf("%d", &metodo);
  switch (metodo) {
  case 1:
    for (int i = 1; i < l; i++) {
      for (int j = 0; j < l - i; j++) {
        if (abs(vector[j]) > abs(vector[j + 1])) {
          temp = vector[j + 1];
          vector[j + 1] = vector[j];
          vector[j] = temp;
        }
      }
    }
    break;
  case 2:
    for (int i = 1; i < l; i++) {
      for (int j = 0; j < l - i; j++) {
        if (vector[j] > vector[j + 1]) {
          temp = vector[j + 1];
          vector[j + 1] = vector[j];
          vector[j] = temp;
        }
      }
    }
    break;
  case 3:
    for (int i = 1; i < l; i++) {
      for (int j = 0; j < l - i; j++) {
        if (abs(vector[j]) < abs(vector[j + 1])) {
          temp = vector[j + 1];
          vector[j + 1] = vector[j];
          vector[j] = temp;
        }
      }
    }
    break;
  case 4:
    for (int i = 1; i < l; i++) {
      for (int j = 0; j < l - i; j++) {
        if (vector[j] < vector[j + 1]) {
          temp = vector[j + 1];
          vector[j + 1] = vector[j];
          vector[j] = temp;
        }
      }
    }
    break;
  default:
    break;
  }

  for (int i = 0; i < l; i++) {
    printf("%.2f ", vector[i]);
  }
  return 0;
}
