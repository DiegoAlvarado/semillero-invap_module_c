#include <stdint.h>
#include <stdio.h>

int main() {
  int i = 102;
  int j = -56;
  long ix = -158693157400;
  unsigned u = 35460;
  float x = 12.687;
  double dx = 0.000000025;
  char c = 'C';

  // A
  printf("%4d ", i);
  printf("%4d ", j);
  printf("%14.8e ", x);
  printf("%14.8e \n\n", dx);
  // B
  printf("%4d \n", i);
  printf("%4d \n", j);
  printf("%14.8e \n", x);
  printf("%14.8e \n\n", dx);
  // C
  printf("%5d \n", i);
  printf("%4d \n", j);
  printf("%12ld \n", ix);
  printf("%10.5f \n", x);
  printf("%5u \n\n", u);
  // D
  printf("%5d ", i);
  printf("%4d ", j);
  printf("%12ld \n\n", ix);
  printf("%10.5f ", x);
  printf("%5u ", u);
  printf("\n\n");
  // E
  printf("%6d   ", i);
  printf("%6u   ", u);
  printf("%6c", c);
  printf("\n\n");
  // F
  printf("%5d ", j);
  printf("%5u ", u);
  printf("%11.4f ", x);
  printf("\n\n");
  // G
  printf("%-5d ", j);
  printf("%-5u ", u);
  printf("%-11.4f ", x);
  printf("\n\n");
  // H
  printf("%+5d ", j);
  printf("%+5u ", u);
  printf("%+11.4f ", x);
  printf("\n\n");
  // I
  printf("%05d ", j);
  printf("%05u ", u);
  printf("%011.4f ", x);
  printf("\n\n");
  // J
  printf("%5d ", j);
  printf("%5u ", u);
  printf("%#11.4f ", x);
  printf("\n\n");

  return 0;
}
