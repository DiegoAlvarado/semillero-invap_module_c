#include <stdbool.h>
#include <stdio.h>
#include <string.h>

int main() {
  char texto[80];
  char inverso[80];
  int cont = 0;
  bool detener = false;
  while (detener != true) {
    printf("Ingrese texto a invertir:\n\t");
    scanf("%s", texto);
    printf("%s\n", texto);

    cont = strlen(texto);
    int i = 0;

    while (texto[i] != '\0') {
      i++;
    }

    printf("Inverso:\n\t");
    for (int j = i - 1, k = 0; j >= 0, k <= i; j--, k++) {
      inverso[k] = texto[j];
    }
    printf("%s\n", inverso);
    for (int i = 0; i < cont; i++) {
      if (texto[i - 1] == '-' && texto[i] == '1') {
        detener = true;
      }
    }
  }
  printf("programa finalizado");
  return 0;
}