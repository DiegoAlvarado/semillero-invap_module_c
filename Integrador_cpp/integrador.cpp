#include "integrador.h"

int main() {

  int LlenarDatos = 0;
  double temp_aux;
  double hum_aux;
  double pres_aux;
  int aux;
  std::vector<double> temp;
  std::vector<double> presion;
  std::vector<double> humedad;
  std::vector<mostrar_datos *> lista;

  sensor_temperatura Itemperatura;
  sensor_humedad Ihumedad;
  sensor_presion Ipresion;

  while (LlenarDatos != 1) {
    std::cout << "Ingrese los datos de (T[C] H[%] P[KPa])" << std::endl;
    std::cin >> temp_aux >> hum_aux >> pres_aux;
    temp.push_back(temp_aux);
    presion.push_back(pres_aux);
    humedad.push_back(hum_aux);
    std::cout << "Para continuar cargando datos presione cualquier numero"
              << std::endl;
    std::cout << "Para Detenerse presione 1" << std::endl;
    std::cin >> LlenarDatos;
  }
  Itemperatura.setearDato(temp);
  Ihumedad.setearDato(humedad);
  Ipresion.setearDato(presion);

  mostrar_datos *condiciones =
      new condiciones_actuales(Itemperatura, Ipresion, Ihumedad);
  mostrar_datos *maxima =
      new estadisticas_de_temperaturas(Itemperatura, Ipresion, Ihumedad);
  mostrar_datos *pronostico =
      new pronostico_simple(Itemperatura, Ipresion, Ihumedad);

  lista.push_back(condiciones);
  lista.push_back(maxima);
  lista.push_back(pronostico);

  std::cout << "Tipos de vista de datos disponibles por defecto" << std::endl;
  for (auto it : lista) {
    std::cout << it->nombre << std::endl;
  }
  std::cout << "ingrese el numero de vista que desea eliminar, de lo contrario "
               "ingrese 0"
            << std::endl;
  std::cin >> aux;
  if (aux == 1 || aux == 2 || aux == 3) {
    lista.erase(lista.begin() + (aux - 1));
  }

  for (auto it : lista) {
    it->imprimir();
  }

  delete condiciones;
  delete maxima;
  delete pronostico;

  return 0;
}
