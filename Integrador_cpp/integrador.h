#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

class sensor {
public:
  sensor() : dato(0) {}
  virtual ~sensor() {}
  virtual void setearDato(std::vector<double>) = 0;
  std::vector<double> dato;
};

class sensor_temperatura : public sensor {
public:
  virtual void setearDato(std::vector<double> temp) override { dato = temp; }
};

class sensor_presion : public sensor {
public:
  virtual void setearDato(std::vector<double> presion) override {
    dato = presion;
  }
};

class sensor_humedad : public sensor {
public:
  virtual void setearDato(std::vector<double> humedad) override {
    dato = humedad;
  }
};

class mostrar_datos {
public:
  mostrar_datos() : temperatura(), presion(), humedad() {}
  mostrar_datos(sensor_temperatura temp, sensor_presion pres,
                sensor_humedad hum)
      : temperatura(temp), presion(pres), humedad(hum) {}

  virtual ~mostrar_datos() {}

  std::string nombre;
  sensor_temperatura temperatura;
  sensor_presion presion;
  sensor_humedad humedad;

  virtual void imprimir() {}
};

class condiciones_actuales : public mostrar_datos {
public:
  condiciones_actuales(sensor_temperatura temp, sensor_presion pres,
                       sensor_humedad hum)
      : mostrar_datos(temp, pres, hum) {
    nombre = "condiciones actuales";
  }

  virtual void imprimir() {
    std::cout << "Current conditions: " << temperatura.dato.back()
              << " degrees and " << humedad.dato.back() << " humidity"
              << std::endl;
  }
};

class estadisticas_de_temperaturas : public mostrar_datos {
public:
  estadisticas_de_temperaturas(sensor_temperatura temp, sensor_presion pres,
                               sensor_humedad hum)
      : mostrar_datos(temp, pres, hum) {
    nombre = "estadisticas de temperatura";
  }

  void promedioTemperaturas() {
    double aux;

    for (int i = 0; i < temperatura.dato.size(); i++) {
      average_temp = average_temp + temperatura.dato[i];
    }
    average_temp = average_temp / (temperatura.dato.size());
  }
  void temperaturaMaximaYMinima() {
    max_temp = temperatura.dato[0];
    min_temp = temperatura.dato[0];
    for (int i = 0; i < temperatura.dato.size(); i++) {
      if (temperatura.dato[i] > temperatura.dato[i - 1]) {
        max_temp = temperatura.dato[i];
      }
      if (temperatura.dato[i] < temperatura.dato[i - 1]) {
        min_temp = temperatura.dato[i];
      }
    }
  }
  virtual void imprimir() {
    promedioTemperaturas();
    temperaturaMaximaYMinima();
    std::cout << "voidAvg/Max/Min temperature = " << average_temp << '/'
              << max_temp << '/' << min_temp << std::endl;
  }

private:
  std::vector<double> temp_aux;
  double average_temp = 0;
  double min_temp;
  double max_temp;
};

class pronostico_simple : public mostrar_datos {
public:
  pronostico_simple(sensor_temperatura temp, sensor_presion pres,
                    sensor_humedad hum)
      : mostrar_datos(temp, pres, hum) {
    nombre = "pronostico simple";
  }

  virtual void imprimir() {
    if (temperatura.dato.back() > 30) {
      std::cout << "El día va a estar caluroso!" << std::endl;
    }
    if (temperatura.dato.back() < 5) {
      std::cout << "Winter is comming" << std::endl;
    }
    if (temperatura.dato.back() > 5 && temperatura.dato.back() < 30) {
      std::cout << "Pedite el día que está ideal para salir a pasear"
                << std::endl;
    }
  }
};
